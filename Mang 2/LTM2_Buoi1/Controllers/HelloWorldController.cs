﻿using System.Web.Mvc;

namespace LTM2_Buoi1.Controllers
{
    public class HelloWorldController : Controller
    {
        // GET: /HelloWorld/Index/id
        public string Index(string id, string id2)
        {
            return "This is my <b>default</b> action...";
        }

        // [Route("{action}/{name}?NumTimes={numTimes}")]
        public ActionResult Welcome(string name, int numTimes = 1)
        {
            // K40&NumTimes=100
            //name = "<script>alert(1)</script>";
            //return //"Hello" + name + ". NumTimes is: " + numtimes
            //HttpUtility.HtmlEncode("Hello " + name +", NumTimes is: " + numTimes);
            ViewBag.Name = "Hello " + name;
            ViewBag.NumTime = numTimes;
            return View();
        }
    }
}