﻿using LTM2_Buoi1.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace LTM2_Buoi1.Controllers
{
    public class MoviesController : Controller
    {
        private MovieDBContext db = new MovieDBContext();

        // GET: Movies
        public ActionResult Index()
        {
            var movies = from m in db.Movies
                         select m;

            ViewBag.ListGenre = db.Movies.

            Select(x => new SelectListItem() { Text = x.Genre, Value = x.Genre }).Distinct().ToList();

            return View(movies);
        }

        public ActionResult Timkiem(string tieude, string genre, decimal? price)
        {
            var movies = from m in db.Movies
                         select m;
            if (!String.IsNullOrEmpty(tieude))
            {
                movies = movies.Where(s => s.Title.Contains(tieude) && s.Genre.Contains(genre) && s.Price >= price);
            }
            return View("Index", movies);
        }

        //public ActionResult Upload(HttpPostedFileBase Picturefile)
        //{
        //    try
        //    {
        //        if (Picturefile.ContentLength > 0)
        //       {
        //            string FileName = Path.GetFileName(Picturefile.FileName);
        //            string path = Path.Combine(Server.MapPath("~/Picture"), FileName);
        //            Picturefile.SaveAs(path);
        //        }
        //        ViewBag.Message = "File uploaded successfully!";
        //        return View();
        //    }
        //    catch
        //    {
        //        ViewBag.Message = "File upload failed";
        //        return View();
        //    }
        //}

        // GET: Movies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // GET: Movies/Create
        public ActionResult Create()
        {
            List<SelectListItem> list = new List<SelectListItem>{
               new SelectListItem{
                   Value="1",
                   Text="Kiem hiep"
               },
               new SelectListItem{
                   Value="2",
                   Text="XHD"
               },
               new SelectListItem{
                   Value="3",
                   Text="Kinh di"
               }
           }.ToList();
            ViewBag.List = list;
            return View();
        }

        // POST: Movies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,ReleaseDate,Genre,Rated,Price,Picture")] Movie movie)
        {
            if (ModelState.IsValid)
            {
                HttpPostedFileBase File = Request.Files["file"];
                db.Movies.Add(movie);
                movie.PictureUpload = File.FileName;
                File.SaveAs(Server.MapPath("~/Picture/" + movie.PictureUpload + ".jpg"));
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(movie);
        }

        // GET: Movies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,ReleaseDate,Genre,Rated,Price,Picture")] Movie movie)
        {
            if (ModelState.IsValid)
            {
                HttpPostedFileBase File = Request.Files["file"];
                db.Movies.Add(movie);
                movie.PictureUpload = File.FileName;
                File.SaveAs(Server.MapPath("~/Picture/" + movie.PictureUpload + ".jpg"));
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(movie);
        }

        // GET: Movies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Movie movie = db.Movies.Find(id);
            db.Movies.Remove(movie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}