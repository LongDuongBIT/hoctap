﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LTM2_Buoi1.Startup))]

namespace LTM2_Buoi1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}