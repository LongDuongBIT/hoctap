﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LTM2_Buoi1.Models
{
    internal class DateTimeAnno : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            DateTime dt = Convert.ToDateTime(value);
            return dt > DateTime.Now;
        }
    }

    public class Movie
    {
        public int ID { get; set; }

        [Display(Name = "Tên")]
        [StringLength(200, MinimumLength = 3)]
        public string Title { get; set; }

        [Display(Name = "Ngày")]
        [DataType(DataType.Date)]
        [DateTimeAnno(ErrorMessage = "Sai")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ReleaseDate { get; set; }

        [Display(Name = "Thể loại")]
        [StringLength(10)]
        public string Genre { get; set; }

        [Display(Name = "Thứ hạng")]
        public double Rated { get; set; }

        [Display(Name = "Giá tiền")]
        public decimal Price { get; set; }

        [Display(Name = "Tóm tắt")]
        [MinLength(10)]
        public string summary { get; set; }

        public byte[] Picture { get; set; }

        [Display(Name = "Hình ảnh")]
        public String PictureUpload { get; set; }

        [ForeignKey("GenreObj")]
        public int? GenreID { get; set; }

        public virtual Genre GenreObj { get; set; }
        public IList<ChiTietHoaDon> ChiTietHoaDon { get; set; }
    }
}