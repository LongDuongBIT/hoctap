﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LTM2_Buoi1.Models
{
    public class ChiTietHoaDon
    {
        [Key]
        [Column(Order = 1)]
        public int MaHoaDon { get; set; }

        [ForeignKey("MaHoaDon")]
        public virtual Hoadon HoaDon { get; set; }

        [Key]
        [Column(Order = 2)]
        public int MaMovie { get; set; }

        [ForeignKey("MaMovie")]
        public virtual Movie Movie { get; set; }

        public string SoLuong { get; set; }
    }
}