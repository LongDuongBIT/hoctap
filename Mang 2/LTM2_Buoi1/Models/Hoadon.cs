﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LTM2_Buoi1.Models
{
    public class Hoadon
    {
        [Key]
        public int MaHoaDon { get; set; }

        public IList<ChiTietHoaDon> ChiTietHoaDon { get; set; }
        public double TongTien { get; set; }
        public DateTime NgayLap { get; set; }

        public int? MaKhachHang { get; set; }
    }
}