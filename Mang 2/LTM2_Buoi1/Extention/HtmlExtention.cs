﻿using System;
using System.Web.Mvc;

namespace LTM2_Buoi1.Extention
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString ShowImage(this HtmlHelper html, byte[] image)
        {
            String img = "";
            if (image != null)
            {
                img = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(image));
            }
            else
            {
                img = "/Picture/";
            }
            return new MvcHtmlString("<img src='" + img + "' style='width: 200px; '/>");
        }
    }
}