namespace LTM2_Buoi1.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class a : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChiTietHoaDons",
                c => new
                {
                    MaHoaDon = c.Int(nullable: false),
                    MaMovie = c.Int(nullable: false),
                    SoLuong = c.String(),
                })
                .PrimaryKey(t => new { t.MaHoaDon, t.MaMovie })
                .ForeignKey("dbo.Hoadons", t => t.MaHoaDon, cascadeDelete: true)
                .ForeignKey("dbo.Movies", t => t.MaMovie, cascadeDelete: true)
                .Index(t => t.MaHoaDon)
                .Index(t => t.MaMovie);

            CreateTable(
                "dbo.Hoadons",
                c => new
                {
                    MaHoaDon = c.Int(nullable: false, identity: true),
                    TongTien = c.Double(nullable: false),
                    NgayLap = c.DateTime(nullable: false),
                    MaKhachHang = c.Int(),
                })
                .PrimaryKey(t => t.MaHoaDon);

            CreateTable(
                "dbo.Movies",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    Title = c.String(maxLength: 200),
                    ReleaseDate = c.DateTime(nullable: false),
                    Genre = c.String(maxLength: 10),
                    Rated = c.Double(nullable: false),
                    Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    summary = c.String(),
                    Picture = c.Binary(),
                    PictureUpload = c.String(),
                    GenreID = c.Int(),
                })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Genres", t => t.GenreID)
                .Index(t => t.GenreID);

            CreateTable(
                "dbo.Genres",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 200),
                })
                .PrimaryKey(t => t.ID);
        }

        public override void Down()
        {
            DropForeignKey("dbo.Movies", "GenreID", "dbo.Genres");
            DropForeignKey("dbo.ChiTietHoaDons", "MaMovie", "dbo.Movies");
            DropForeignKey("dbo.ChiTietHoaDons", "MaHoaDon", "dbo.Hoadons");
            DropIndex("dbo.Movies", new[] { "GenreID" });
            DropIndex("dbo.ChiTietHoaDons", new[] { "MaMovie" });
            DropIndex("dbo.ChiTietHoaDons", new[] { "MaHoaDon" });
            DropTable("dbo.Genres");
            DropTable("dbo.Movies");
            DropTable("dbo.Hoadons");
            DropTable("dbo.ChiTietHoaDons");
        }
    }
}