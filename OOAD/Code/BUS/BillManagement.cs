﻿using DTO;
using System;
using System.Data;

namespace BUS
{
    public class BillManagement
    {
        private DAO.BillManagement dao_bill_management;

        public BillManagement()
        {
            dao_bill_management = new DAO.BillManagement();
        }

        public static Bill CreateNewBill(Bill new_bill)
        {
            new_bill.CreatingTime = DateTime.Now;
            var bill = DAO.BillManagement.CreateNewBill(new_bill);
            if (new_bill.Customer.Type == "Thân thiết")
            {
                //Cộng điểm
                int new_point = bill.Customer.Point + (int)(bill.ActualTotal / 1000);
                DAO.BillManagement.UpdatePoint(bill.Customer, new_point);

                //Nếu điểm cao hơn 5000 thì nâng cấp khách hàng
                if (new_point >= 5000)
                {
                    DAO.BillManagement.UpgradeCustomerToVIP(bill.Customer);
                    bill.Customer.Type = "VIP";
                }
            }
            return bill;
        }

        public static Customer GetCustomerDetail(string customer_id) => DAO.BillManagement.GetCustomerDetail(customer_id);

        public DataTable LoadBeverageList(string beverageName)
        {
            var beverageList = DAO.BillManagement.LoadBeverageList(beverageName);
            var dt = new DataTable();
            dt.Columns.Add("Mã thức uống");
            dt.Columns.Add("Tên thức uống");
            dt.Columns.Add("Giá bán");
            dt.Columns.Add("Loại");
            foreach (Product beverage in beverageList)
            {
                dt.Rows.Add(beverage.ID, beverage.Name, VNDfromNumber(beverage.Price), beverage.Type);
            }
            return dt;
        }

        public DataTable LoadFoodList(string foodName)
        {
            var foodList = DAO.BillManagement.LoadFoodList(foodName);
            var dt = new DataTable();
            dt.Columns.Add("Mã món ăn");
            dt.Columns.Add("Tên món ăn");
            dt.Columns.Add("Giá bán");
            dt.Columns.Add("Loại");
            foreach (Product food in foodList)
            {
                dt.Rows.Add(food.ID, food.Name, VNDfromNumber(food.Price), food.Type);
            }
            return dt;
        }

        //Hàm chuyển đổi từ Số thành Tiền tệ
        private static string VNDfromNumber(int number) => number.ToString("C0", new System.Globalization.CultureInfo("vi-VN"));
    }
}