﻿using System;
using System.Data;

namespace BUS
{
    public class Login
    {
        public static int CheckUsername(string tendangnhap)
        {
            var dao = new DAO.Login();
            return DAO.Login.CheckUsername(tendangnhap);
        }

        public static Tuple<DataTable, int> CheckLogin(string tendangnhap, string matkhau)
        {
            var dao = new DAO.Login();
            return DAO.Login.CheckLogin(tendangnhap, matkhau);
        }
    }
}