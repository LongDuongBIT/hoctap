﻿using DTO;
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;

namespace QLNHDN
{
    public partial class frmQLNHDN : Form
    {
        private Bill[] bill;
        private Staff current_staff;
        private int currentBillIndex;
        private readonly DataTable dtNhanVien;
        private readonly DataTable dtThongTinChiTiet_KTTHHT = new DataTable();
        private frmDangNhap fDN;

        public frmQLNHDN(frmDangNhap _fDN, DataTable _dtNhanVien)
        {
            InitializeComponent();

            fDN = _fDN;
            dtNhanVien = _dtNhanVien;

            lbNhanVien.Text = dtNhanVien.Rows[0].Field<string>(1) + "!";
            current_staff = new Staff(dtNhanVien.Rows[0].Field<int>(0).ToString()); // bỏ mã nhân viên vào current staff
            InitializeBillList();

            foreach (Control c in this.Controls)
            {
                Console.WriteLine(c.Name);
                GetChildControls(c.Controls);
            }
        }

        private void AddProductToBill(DataGridView p_gridProduct, DataGridView p_gridBill, TextBox p_Quantity)
        {
            errorProvider.Clear();
            try
            {
                var dt = (DataTable)p_gridBill.DataSource;
                string product_ID = p_gridProduct.SelectedRows[0].Cells[0].Value.ToString();
                string product_Name = p_gridProduct.SelectedRows[0].Cells[1].Value.ToString();
                string product_Price = p_gridProduct.SelectedRows[0].Cells[2].Value.ToString();
                int product_Quantity = Convert.ToInt16(p_Quantity.Text);
                string product_TypeName = p_gridProduct.SelectedRows[0].Cells[3].Value.ToString();

                //Nếu SP đã có trong hoá đơn thì cộng thêm số lượng
                bool already_added = false;
                foreach (DataRow row in dt.Rows)
                {
                    if (row.ItemArray[0].ToString() == product_ID)
                    {
                        int quantity = Convert.ToInt32(row.ItemArray[3].ToString());
                        quantity += Convert.ToInt16(product_Quantity);
                        row.SetField(3, quantity);
                        row.SetField(4, VNDfromNumber(quantity * (NumberFromVND(product_Price))));
                        already_added = true;
                        break;
                    }
                }
                //Nếu chưa thì thêm một dòng mới vào bảng
                if (!already_added)
                {
                    string product_SumPrice = VNDfromNumber(NumberFromVND(product_Price) * Convert.ToInt32(product_Quantity));
                    dt.Rows.Add(product_ID, product_Name, product_Price, product_Quantity,
                        product_SumPrice, product_TypeName);
                }
                p_gridBill.DataSource = dt;
                p_gridBill.Refresh();

                //Đổi màu nút Hoá đơn sang đỏ
                ChangeBillIcon(2);

                //Tính lại tổng Hoá đơn
                btnCalculateSum_Click(new object(), new EventArgs());
            }
            catch (ArgumentOutOfRangeException)
            {
                errorProvider.SetError(p_gridProduct, "Chưa chọn món ăn/thức uống!");
            }
            catch (FormatException)
            {
                errorProvider.SetError(p_Quantity, "Chưa nhập số lượng!");
            }
        }

        private void BillRefresh()
        {
            bill[currentBillIndex] = new Bill();
            bill[currentBillIndex].Staff = current_staff;
            ShowBill(currentBillIndex);
            ChangeBillIcon(1);
        }

        private void btnAddBeverage_Click(object sender, EventArgs e)
        {
            AddProductToBill(gridBeverage, gridBillDetail, txtBeverageQuantity);
            txtBeverageQuantity.Text = "1";
            txtBeverageSearch.Text = "";
            btnBeverageSearch_Click(sender, e);
        }

        private void btnAddFood_Click(object sender, EventArgs e)
        {
            AddProductToBill(gridFood, gridBillDetail, txtFoodQuantity);
            txtFoodQuantity.Text = "1";
            txtFoodSearching.Text = "";
            btnFoodSearching_Click(sender, e);
        }

        private void btnBeverageSearch_Click(object sender, EventArgs e)
        {
            string keyword = txtBeverageSearch.Text;
            var bill = new BUS.BillManagement();
            gridBeverage.DataSource = bill.LoadBeverageList(keyword);
            gridBeverage.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
        }

        private void btnBill_Click(object sender, EventArgs e)
        {
            int bill_index = Convert.ToInt16(((Button)sender).Name.Substring(7));
            ShowBill(bill_index - 1);
        }

        private void btnBillPrinting_Click(object sender, EventArgs e)
        {
            if (bill[currentBillIndex].DetailTable.Rows.Count == 0)
            {
                MessageBox.Show("Hoá đơn rỗng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                printPreviewDialog.Document = printDoc;
                printPreviewDialog.ShowDialog();
            }
        }

        private void btnBillRefresh_Click(object sender, EventArgs e)
        {
            var dr = MessageBox.Show("Bạn có muốn làm mới hoá đơn này?", "Xác nhận", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dr == DialogResult.OK)
            {
                BillRefresh();
            }
        }

        private void btnCalculateSum_Click(object sender, EventArgs e)
        {
            int total = 0;
            foreach (DataRow row in bill[currentBillIndex].DetailTable.Rows)
            {
                int productSumPrice = NumberFromVND(row.Field<string>(4));
                total += productSumPrice;
            }
            double discountRate = Convert.ToDouble(txtDiscountRate.Text);
            int discountAmount = (int)(total * discountRate);
            int actualTotal = total - discountAmount;

            txtTotal.Text = VNDfromNumber(total);
            txtDiscountAmount.Text = VNDfromNumber(discountAmount);
            txtActualTotal.Text = VNDfromNumber(actualTotal);

            bill[currentBillIndex].Total = total;
            bill[currentBillIndex].DiscountAmount = discountAmount;
            bill[currentBillIndex].ActualTotal = actualTotal;
        }

        private void btnChangeCustomerID_Click(object sender, EventArgs e)
        {
            int number;
            if (txtCustomerID.Text == null || !int.TryParse(txtCustomerID.Text, out number))
            {
                MessageBox.Show("Mã khách hàng không hợp lệ!", "Lỗi", MessageBoxButtons.OK);
                txtCustomerID.Text = "1";
                btnChangeCustomerID_Click(sender, e);
            }
            else
            {
                var bus = new BUS.BillManagement();
                var customer = BUS.BillManagement.GetCustomerDetail(txtCustomerID.Text);
                if (customer.ID == null)
                {
                    MessageBox.Show("Không tìm thấy mã khách hàng!", "Lỗi", MessageBoxButtons.OK);
                    txtCustomerID.Text = "1";
                    btnChangeCustomerID_Click(sender, e);
                }
                else
                {
                    txtCustomerID.Text = customer.ID;
                    txtDiscountRate.Text = customer.DiscountRate.ToString();
                    bill[currentBillIndex].Customer = customer;
                }
            }
            btnCalculateSum_Click(sender, e);
        }

        private void btnCustomerDetail_Click(object sender, EventArgs e)
        {
            var bus = new BUS.BillManagement();
            var customer = BUS.BillManagement.GetCustomerDetail(txtCustomerID.Text);
            var sb_customer_info = new StringBuilder();
            sb_customer_info.AppendLine("Mã KH: " + customer.ID);
            sb_customer_info.AppendLine("Họ tên KH: " + customer.FullName);
            sb_customer_info.AppendLine("CMND: " + customer.CivilID);
            sb_customer_info.AppendLine("Số điện thoại: " + customer.PhoneNumber);
            sb_customer_info.AppendLine("Cấp độ khách hàng: " + customer.Type);
            sb_customer_info.AppendLine("Tỉ lệ chiết khấu: " + customer.DiscountRate);
            sb_customer_info.AppendLine("Điểm tích luỹ: " + customer.Point);

            MessageBox.Show(sb_customer_info.ToString());
        }

        private void btnFoodSearching_Click(object sender, EventArgs e)
        {
            var keyword = txtFoodSearching.Text;
            var bill = new BUS.BillManagement();
            gridFood.DataSource = bill.LoadFoodList(keyword);
            gridFood.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
        }

        private void btnSettlePay_Click(object sender, EventArgs e)
        {
            if (bill[currentBillIndex].DetailTable.Rows.Count == 0)
            {
                MessageBox.Show("Hoá đơn rỗng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (bill[currentBillIndex].ID == "")
                {
                    var dr = MessageBox.Show("Bạn có muốn thanh toán hoá đơn này?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        var bus = new BUS.BillManagement();
                        var settled_bill = new Bill();
                        settled_bill = BUS.BillManagement.CreateNewBill(bill[currentBillIndex]);

                        //In hoá đơn
                        btnBillPrinting_Click(btnBillPrinting, e);

                        //Thêm mới khách hàng thân thiết
                        int actualTotal = NumberFromVND(txtActualTotal.Text);
                        if (txtCustomerID.Text == "1" && actualTotal >= 500000)
                        {
                            MessageBox.Show("Tạo hoá đơn thành công!");
                            MessageBox.Show("Hoá đơn có giá trị trên 500.000đ, bạn nên đăng ký Khách hàng thân thiết mới.", "Đăng ký khách hàng thân thiết", MessageBoxButtons.OK);
                        }
                        //Thông báo khách hàng lên mức "Khách hàng VIP"
                        else if (settled_bill.Customer.Type == "VIP" && txtDiscountRate.Text == "0.1")
                        {
                            MessageBox.Show("Tạo hoá đơn thành công!");
                            MessageBox.Show("Chúc mừng khách hàng đã được nâng cấp thành VIP", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        //Làm mới hoá đơn
                        BillRefresh();
                    }
                }
                else
                {
                    MessageBox.Show("Hoá đơn đã được thanh toán!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            ShowBill(currentBillIndex);
        }

        private void ChangeBillIcon(int ImageIndex)
        {
            var button = (Button)this.Controls.Find(String.Format("btnBill{0}", currentBillIndex + 1), true)[0];
            button.ImageIndex = ImageIndex;
        }

        private void frmQLNHDN_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Bạn có chắc chắn muốn thoát?", "Xác nhận!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                fDN.Show();
                fDN.reload();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void frmQLNHDN_Load(object sender, EventArgs e)
        {
            tabMenu.TabPages.Clear();
            lblTitle.Text = "Trang chủ";
            HienMotTab(tabMenu, tabBillManagement);
            //Nếu hoá đơn hiện tại đã thanh toán, làm mới hoá đơn.
            if (bill[currentBillIndex].ID != "")
            {
                BillRefresh();
            }
        }

        private string GenerateBillPrintingContent()
        {
            var sb = new StringBuilder();
            var currentBill = bill[currentBillIndex];
            sb.AppendFormat("{0, 40}\n", "Nhà hàng BIT".ToUpper());

            //Nếu chưa lưu HĐ xuống CSDL thì xuất hoá đơn tạm.
            if (currentBill.ID == "")
            {
                sb.AppendFormat("{0, 35}\n", @"[HOÁ ĐƠN TẠM]");
            }
            else
            {
                sb.AppendFormat("{0, 40}\n", @"HOÁ ĐƠN CHÍNH THỨC");
                sb.AppendFormat("Mã hoá đơn: {0}\n", currentBill.ID);
            }

            //Nếu khách hàng bình thường thì không cần in Mã KH và cấp độ khách hàng
            if (currentBill.Customer.Type != "Bình thường")
            {
                sb.AppendFormat("Mã khách hàng: {0,-5} ", currentBill.Customer.ID);
                sb.AppendFormat("{0,20}:", "Cấp độ khách hàng");
                if (currentBill.Customer.Type == "VIP" && txtDiscountRate.Text == "0.1")
                {
                    sb.AppendFormat(" {0,-15}\n", "Thân thiết");
                }
                else
                {
                    sb.AppendFormat(" {0,-15}\n", currentBill.Customer.Type);
                }
            }

            sb.AppendFormat("Mã nhân viên: {0}\n", currentBill.Staff.ID);
            sb.AppendFormat("Thời gian: {0:d} {0:t}\n", currentBill.CreatingTime);
            sb.AppendLine();
            sb.AppendFormat("{0,20}{1,3}{2,5}{3,2}{4,20}\n", "Đơn giá", "", "SL", "", "Thành tiền");
            sb.AppendLine("---------------------------------------------------------");
            foreach (DataRow row in currentBill.DetailTable.Rows)
            {
                sb.AppendLine(row.Field<string>("Tên SP"));
                string productDetail = String.Format("{0,20}{1,3}{2,5}{3,2}{4,20}",
                    row.Field<string>("Đơn giá"), "x", row.Field<int>("Số lượng"), "=", row.Field<string>("Thành tiền"));
                sb.AppendLine(productDetail);
            }
            sb.AppendLine("---------------------------------------------------------");
            sb.AppendFormat("{0,30}{1,20}\n", "Tổng tiền", txtTotal.Text);
            string discountRate = (Convert.ToDouble(txtDiscountRate.Text) * 100).ToString();
            sb.AppendFormat("{0,30}({1,2}%){2,15}\n", "Tiền chiết khấu", discountRate, txtDiscountAmount.Text);
            sb.AppendFormat("{0,30}{1,20}\n", "THANH TOÁN", txtActualTotal.Text);
            sb.AppendLine();
            if (txtDiscountRate.Text == "0.1")
            {
                sb.AppendFormat("{0,30}{1,20}\n", "Điểm hiện tại", currentBill.Customer.Point);
                sb.AppendFormat("{0,30}{1,20}\n", "Điểm được cộng", (int)(currentBill.ActualTotal / 1000));
                sb.AppendFormat("{0,30}{1,20}\n", "Điểm sau khi được cộng", currentBill.Customer.Point + (int)(currentBill.ActualTotal / 1000));

                if (currentBill.Customer.Point + (int)(currentBill.ActualTotal / 1000) >= 5000)
                {
                    sb.AppendLine("Chúc mừng thực khách vừa được nâng cấp lên thành \"VIP\"!");
                }
            }
            sb.AppendLine();
            sb.AppendFormat("{0, 40}", "Hẹn gặp lại quý thực khách!".ToUpper());
            return sb.ToString();
        }

        private static void GetChildControls(Control.ControlCollection cl)
        {
            foreach (Control c in cl)
            {
                Console.WriteLine("{0}", c.Name);
            }
        }

        private void gridBillDetail_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //Xoá thông báo lỗi
            var dgv = ((DataGridView)sender);
            dgv.Rows[e.RowIndex].ErrorText = "";

            //Tính lại thành tiền dựa trên số lương mới
            var row = ((DataTable)dgv.DataSource).Rows[e.RowIndex];
            int quantity = Convert.ToInt32(row.ItemArray[3].ToString());
            int productPrice = NumberFromVND(row.ItemArray[2].ToString());
            row.SetField(3, quantity);
            row.SetField(4, VNDfromNumber(quantity * productPrice));
            btnCalculateSum_Click(sender, e);
        }

        //private static void gridBillDetail_DataError(object sender, DataGridViewDataErrorEventArgs e)
        //{
        //    var dgv = ((DataGridView)sender);
        //    dgv.Rows[e.RowIndex].ErrorText = "Dữ liệu không hợp lệ!";
        //    e.Cancel = true;
        //}

        private void gridBillDetail_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var dgv = ((DataGridView)sender);
                int row_index = dgv.HitTest(e.X, e.Y).RowIndex;
                dgv.ClearSelection();
                if (row_index == -1)
                {
                    return;
                }

                dgv.Rows[row_index].Selected = true;
                contextBillItemSelection.Show(dgv, e.X, e.Y);
            }
        }

        private void HienMotTab(TabControl tab_menu, TabPage tab)
        {
            tab_menu.TabPages.Clear();
            tab_menu.TabPages.Add(tab);
            lblTitle.Text = tab.Text;
        }

        private void InitializeBillList()
        {
            const int bill_count = 24;
            bill = new Bill[bill_count];
            for (int i = 0; i < bill_count; i++)
            {
                bill[i] = new Bill();
                bill[i].Staff = current_staff;
            }
            currentBillIndex = 0;
            ShowBill(0);
        }

        private void itemDelete_Click(object sender, EventArgs e)
        {
            var button = MessageBox.Show("Bạn muốn xoá dòng này?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (button == DialogResult.Yes)
            {
                gridBillDetail.Rows.Remove(gridBillDetail.SelectedRows[0]);
                btnCalculateSum_Click(sender, e);
            }
        }

        //Hàm chuyển đổi từ Tiền tệ thành Số
        private static int NumberFromVND(string currency) => int.Parse(currency, System.Globalization.NumberStyles.Currency, new System.Globalization.CultureInfo("vi-VN"));

        //private void PrintBill()
        //{
        //    using (var print_doc = new PrintDocument())
        //    {
        //        print_doc.DefaultPageSettings.Landscape = true; //set portrait orientation to print page
        //        print_doc.DefaultPageSettings.Margins = new Margins(50, 50, 10, 10);    //100 = 1 inch = 2.54 cm
        //        print_doc.DocumentName = String.Format(@"{0}_{1}", DateTime.Now.ToString("yyyyMMdd"), bill[currentBillIndex].ID);
        //    }
        //}

        private void printDoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            using (var font = new Font("Courier New", 12))
            {
                e.Graphics.DrawString(GenerateBillPrintingContent(), font, Brushes.Black, e.MarginBounds);
            }
        }

        private void ShowBill(int index)
        {
            currentBillIndex = index;
            gridBillDetail.DataSource = bill[index].DetailTable;
            gridBillDetail.Refresh();

            gridBillDetail.Columns["Tên SP"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //Không cho chỉnh sửa nội dung tất cả các cột, trừ cột "Số lượng"
            foreach (DataGridViewColumn col in gridBillDetail.Columns)
            {
                if (col.HeaderText != "Số lượng")
                {
                    col.ReadOnly = true;
                }
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            txtCustomerID.Text = bill[index].Customer.ID;
            btnChangeCustomerID_Click(new object(), new EventArgs());
            lblBillTitle.Text = String.Format("Hoá đơn {0}", index + 1);

            //Đổi màu nút khi hoá đơn được chọn
            foreach (Button button in grpbxBillButtonList.Controls)
            {
                if (button.Name.Equals(String.Format("btnBill{0}", currentBillIndex + 1)))
                    button.BackColor = Color.White;
                else
                    button.BackColor = Color.Transparent;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbGioHienTai.Text = DateTime.Now.ToLongTimeString();
            lbNgayHienTai.Text = DateTime.Now.ToShortDateString();
        }

        private void txtBeverageSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Enter)
            {
                btnBeverageSearch_Click(sender, e);
            }
        }

        private void txtCustomerID_Enter(object sender, EventArgs e)
        {
            btnChangeCustomerID_Click(sender, e);
        }

        private void txtCustomerID_Leave(object sender, EventArgs e)
        {
            btnChangeCustomerID_Click(sender, e);
        }

        private void txtFoodSearching_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Enter)
            {
                btnFoodSearching_Click(sender, e);
            }
        }

        //Hàm chuyển đổi từ Số thành Tiền tệ
        private static string VNDfromNumber(int number) => number.ToString("C0", new System.Globalization.CultureInfo("vi-VN"));
    }
}