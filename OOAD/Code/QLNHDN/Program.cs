﻿using System;
using System.Windows.Forms;

namespace QLNHDN
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (var frmDangNhap1 = new frmDangNhap())
            {
                Application.Run(frmDangNhap1);
            }
        }
    }
}