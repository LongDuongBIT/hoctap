﻿namespace QLNHDN
{
    partial class frmQLNHDN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQLNHDN));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Icon30 = new System.Windows.Forms.ImageList(this.components);
            this.Icon50 = new System.Windows.Forms.ImageList(this.components);
            this.Icon10 = new System.Windows.Forms.ImageList(this.components);
            this.lblXemTKTheo = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.contextBillItemSelection = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.itemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbNhanVien = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.lbGioHienTai = new System.Windows.Forms.Label();
            this.lbNgayHienTai = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.menuTopBar = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.printDoc = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.tabBillManagement = new System.Windows.Forms.TabPage();
            this.tabBillList = new System.Windows.Forms.TabControl();
            this.tpBillList1 = new System.Windows.Forms.TabPage();
            this.grpbxBillButtonList = new System.Windows.Forms.GroupBox();
            this.btnBill12 = new System.Windows.Forms.Button();
            this.btnBill11 = new System.Windows.Forms.Button();
            this.btnBill10 = new System.Windows.Forms.Button();
            this.btnBill9 = new System.Windows.Forms.Button();
            this.btnBill8 = new System.Windows.Forms.Button();
            this.btnBill7 = new System.Windows.Forms.Button();
            this.btnBill6 = new System.Windows.Forms.Button();
            this.btnBill5 = new System.Windows.Forms.Button();
            this.btnBill4 = new System.Windows.Forms.Button();
            this.btnBill3 = new System.Windows.Forms.Button();
            this.btnBill2 = new System.Windows.Forms.Button();
            this.btnBill1 = new System.Windows.Forms.Button();
            this.tpBillList2 = new System.Windows.Forms.TabPage();
            this.grpbxBillList2 = new System.Windows.Forms.GroupBox();
            this.btnBill24 = new System.Windows.Forms.Button();
            this.btnBill23 = new System.Windows.Forms.Button();
            this.btnBill22 = new System.Windows.Forms.Button();
            this.btnBill21 = new System.Windows.Forms.Button();
            this.btnBill20 = new System.Windows.Forms.Button();
            this.btnBill19 = new System.Windows.Forms.Button();
            this.btnBill18 = new System.Windows.Forms.Button();
            this.btnBill17 = new System.Windows.Forms.Button();
            this.btnBill16 = new System.Windows.Forms.Button();
            this.btnBill15 = new System.Windows.Forms.Button();
            this.btnBill14 = new System.Windows.Forms.Button();
            this.btnBill13 = new System.Windows.Forms.Button();
            this.grpBill = new System.Windows.Forms.GroupBox();
            this.lblBillTitle = new System.Windows.Forms.Label();
            this.gbGiaTriHD = new System.Windows.Forms.GroupBox();
            this.btnCalculateSum = new System.Windows.Forms.Button();
            this.txtActualTotal = new System.Windows.Forms.TextBox();
            this.txtDiscountAmount = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.lblActualTotal = new System.Windows.Forms.Label();
            this.lblDiscountAmount = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.btnBillRefresh = new System.Windows.Forms.Button();
            this.btnBillPrinting = new System.Windows.Forms.Button();
            this.grpbxCustomerInfo = new System.Windows.Forms.GroupBox();
            this.btnCustomerDetail = new System.Windows.Forms.Button();
            this.btnChangeCustomerID = new System.Windows.Forms.Button();
            this.txtDiscountRate = new System.Windows.Forms.TextBox();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.lblCustomerID = new System.Windows.Forms.Label();
            this.btnSettlePay = new System.Windows.Forms.Button();
            this.gridBillDetail = new System.Windows.Forms.DataGridView();
            this.label20 = new System.Windows.Forms.Label();
            this.tabAddToBill = new System.Windows.Forms.TabControl();
            this.tabAddFood = new System.Windows.Forms.TabPage();
            this.btnFoodSearching = new System.Windows.Forms.Button();
            this.txtFoodSearching = new System.Windows.Forms.TextBox();
            this.lblFoodSearching = new System.Windows.Forms.Label();
            this.btnAddFood = new System.Windows.Forms.Button();
            this.txtFoodQuantity = new System.Windows.Forms.TextBox();
            this.lblAddFood = new System.Windows.Forms.Label();
            this.gridFood = new System.Windows.Forms.DataGridView();
            this.tabAddBeverage = new System.Windows.Forms.TabPage();
            this.btnBeverageSearch = new System.Windows.Forms.Button();
            this.txtBeverageSearch = new System.Windows.Forms.TextBox();
            this.lblBeverageSearch = new System.Windows.Forms.Label();
            this.btnAddBeverage = new System.Windows.Forms.Button();
            this.txtBeverageQuantity = new System.Windows.Forms.TextBox();
            this.lblBeverageQuantity = new System.Windows.Forms.Label();
            this.gridBeverage = new System.Windows.Forms.DataGridView();
            this.tabMenu = new System.Windows.Forms.TabControl();
            this.contextBillItemSelection.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.menuTopBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.tabBillManagement.SuspendLayout();
            this.tabBillList.SuspendLayout();
            this.tpBillList1.SuspendLayout();
            this.grpbxBillButtonList.SuspendLayout();
            this.tpBillList2.SuspendLayout();
            this.grpbxBillList2.SuspendLayout();
            this.grpBill.SuspendLayout();
            this.gbGiaTriHD.SuspendLayout();
            this.grpbxCustomerInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBillDetail)).BeginInit();
            this.tabAddToBill.SuspendLayout();
            this.tabAddFood.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridFood)).BeginInit();
            this.tabAddBeverage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBeverage)).BeginInit();
            this.tabMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // Icon30
            // 
            this.Icon30.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Icon30.ImageStream")));
            this.Icon30.TransparentColor = System.Drawing.Color.Transparent;
            this.Icon30.Images.SetKeyName(0, "images.png");
            this.Icon30.Images.SetKeyName(1, "table-xxl.png");
            this.Icon30.Images.SetKeyName(2, "table-xxl1.png");
            this.Icon30.Images.SetKeyName(3, "BBQ.png");
            this.Icon30.Images.SetKeyName(4, "Drink.jpg");
            // 
            // Icon50
            // 
            this.Icon50.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Icon50.ImageStream")));
            this.Icon50.TransparentColor = System.Drawing.Color.Transparent;
            this.Icon50.Images.SetKeyName(0, "Check Icon.png");
            this.Icon50.Images.SetKeyName(1, "Delete.png");
            this.Icon50.Images.SetKeyName(2, "table-xxl1.png");
            this.Icon50.Images.SetKeyName(3, "images.png");
            this.Icon50.Images.SetKeyName(4, "money-circle-green-3-512.png");
            this.Icon50.Images.SetKeyName(5, "BBQ_icon.png");
            this.Icon50.Images.SetKeyName(6, "Beverage_icon.jpg");
            this.Icon50.Images.SetKeyName(7, "Bill_icon.png");
            this.Icon50.Images.SetKeyName(8, "document-icon.png");
            this.Icon50.Images.SetKeyName(9, "minus-2-256.png");
            this.Icon50.Images.SetKeyName(10, "plus-icon-19.png");
            this.Icon50.Images.SetKeyName(11, "save-xxl.png");
            this.Icon50.Images.SetKeyName(12, "Complete_icon.png");
            // 
            // Icon10
            // 
            this.Icon10.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Icon10.ImageStream")));
            this.Icon10.TransparentColor = System.Drawing.Color.Transparent;
            this.Icon10.Images.SetKeyName(0, "Search.png");
            // 
            // lblXemTKTheo
            // 
            this.lblXemTKTheo.AutoSize = true;
            this.lblXemTKTheo.Location = new System.Drawing.Point(50, 55);
            this.lblXemTKTheo.Name = "lblXemTKTheo";
            this.lblXemTKTheo.Size = new System.Drawing.Size(72, 17);
            this.lblXemTKTheo.TabIndex = 10;
            this.lblXemTKTheo.Text = "Xem theo:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(229, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "đến ngày:";
            // 
            // contextBillItemSelection
            // 
            this.contextBillItemSelection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemDelete});
            this.contextBillItemSelection.Name = "contextBillItemSelection";
            this.contextBillItemSelection.Size = new System.Drawing.Size(95, 26);
            // 
            // itemDelete
            // 
            this.itemDelete.Name = "itemDelete";
            this.itemDelete.Size = new System.Drawing.Size(94, 22);
            this.itemDelete.Text = "Xoá";
            this.itemDelete.Click += new System.EventHandler(this.itemDelete_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.ImageKey = "(none)";
            this.button2.ImageList = this.Icon50;
            this.button2.Location = new System.Drawing.Point(707, 518);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(65, 29);
            this.button2.TabIndex = 48;
            this.button2.Text = "Huỷ HĐ";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button3.ImageKey = "Bill_icon.png";
            this.button3.ImageList = this.Icon50;
            this.button3.Location = new System.Drawing.Point(743, 437);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(130, 77);
            this.button3.TabIndex = 55;
            this.button3.Text = "In hoá đơn";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(224, -46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(101, 26);
            this.label19.TabIndex = 43;
            this.label19.Text = "Hoá đơn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Xin chào";
            // 
            // lbNhanVien
            // 
            this.lbNhanVien.AutoSize = true;
            this.lbNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNhanVien.Location = new System.Drawing.Point(75, 16);
            this.lbNhanVien.Name = "lbNhanVien";
            this.lbNhanVien.Size = new System.Drawing.Size(37, 15);
            this.lbNhanVien.TabIndex = 4;
            this.lbNhanVien.Text = "User";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Controls.Add(this.lbGioHienTai);
            this.groupBox1.Controls.Add(this.lbNgayHienTai);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lbNhanVien);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(1082, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(267, 66);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(200, 18);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(35, 13);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Thoát";
            // 
            // lbGioHienTai
            // 
            this.lbGioHienTai.AutoSize = true;
            this.lbGioHienTai.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGioHienTai.Location = new System.Drawing.Point(81, 43);
            this.lbGioHienTai.Name = "lbGioHienTai";
            this.lbGioHienTai.Size = new System.Drawing.Size(52, 15);
            this.lbGioHienTai.TabIndex = 8;
            this.lbGioHienTai.Text = "8:37 pm";
            // 
            // lbNgayHienTai
            // 
            this.lbNgayHienTai.AutoSize = true;
            this.lbNgayHienTai.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNgayHienTai.Location = new System.Drawing.Point(166, 43);
            this.lbNgayHienTai.Name = "lbNgayHienTai";
            this.lbNgayHienTai.Size = new System.Drawing.Size(69, 15);
            this.lbNgayHienTai.TabIndex = 7;
            this.lbNgayHienTai.Text = "24/12/2016";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Thời gian:";
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 31F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Blue;
            this.lblTitle.Location = new System.Drawing.Point(372, 37);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(555, 48);
            this.lblTitle.TabIndex = 3;
            this.lblTitle.Text = "Quản lý nhà hàng đồ nướng";
            // 
            // menuTopBar
            // 
            this.menuTopBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.menuTopBar.Name = "contextMenuStrip2";
            this.menuTopBar.Size = new System.Drawing.Size(142, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(141, 22);
            this.toolStripMenuItem1.Text = "Tạo hoá đơn";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(141, 22);
            this.toolStripMenuItem2.Text = "Huỷ bàn";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // printDoc
            // 
            this.printDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDoc_PrintPage);
            // 
            // printPreviewDialog
            // 
            this.printPreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog.Enabled = true;
            this.printPreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog.Icon")));
            this.printPreviewDialog.Name = "printPreviewDialog";
            this.printPreviewDialog.Visible = false;
            // 
            // tabBillManagement
            // 
            this.tabBillManagement.Controls.Add(this.tabBillList);
            this.tabBillManagement.Controls.Add(this.grpBill);
            this.tabBillManagement.Controls.Add(this.tabAddToBill);
            this.tabBillManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabBillManagement.Location = new System.Drawing.Point(4, 22);
            this.tabBillManagement.Name = "tabBillManagement";
            this.tabBillManagement.Padding = new System.Windows.Forms.Padding(3);
            this.tabBillManagement.Size = new System.Drawing.Size(1336, 604);
            this.tabBillManagement.TabIndex = 0;
            this.tabBillManagement.Text = "Tính tiền";
            this.tabBillManagement.UseVisualStyleBackColor = true;
            // 
            // tabBillList
            // 
            this.tabBillList.Controls.Add(this.tpBillList1);
            this.tabBillList.Controls.Add(this.tpBillList2);
            this.tabBillList.Location = new System.Drawing.Point(3, 14);
            this.tabBillList.Name = "tabBillList";
            this.tabBillList.SelectedIndex = 0;
            this.tabBillList.Size = new System.Drawing.Size(345, 235);
            this.tabBillList.TabIndex = 7;
            // 
            // tpBillList1
            // 
            this.tpBillList1.Controls.Add(this.grpbxBillButtonList);
            this.tpBillList1.Location = new System.Drawing.Point(4, 22);
            this.tpBillList1.Name = "tpBillList1";
            this.tpBillList1.Padding = new System.Windows.Forms.Padding(3);
            this.tpBillList1.Size = new System.Drawing.Size(337, 209);
            this.tpBillList1.TabIndex = 0;
            this.tpBillList1.Text = "Danh sách 1";
            this.tpBillList1.UseVisualStyleBackColor = true;
            // 
            // grpbxBillButtonList
            // 
            this.grpbxBillButtonList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grpbxBillButtonList.Controls.Add(this.btnBill12);
            this.grpbxBillButtonList.Controls.Add(this.btnBill11);
            this.grpbxBillButtonList.Controls.Add(this.btnBill10);
            this.grpbxBillButtonList.Controls.Add(this.btnBill9);
            this.grpbxBillButtonList.Controls.Add(this.btnBill8);
            this.grpbxBillButtonList.Controls.Add(this.btnBill7);
            this.grpbxBillButtonList.Controls.Add(this.btnBill6);
            this.grpbxBillButtonList.Controls.Add(this.btnBill5);
            this.grpbxBillButtonList.Controls.Add(this.btnBill4);
            this.grpbxBillButtonList.Controls.Add(this.btnBill3);
            this.grpbxBillButtonList.Controls.Add(this.btnBill2);
            this.grpbxBillButtonList.Controls.Add(this.btnBill1);
            this.grpbxBillButtonList.Location = new System.Drawing.Point(6, 7);
            this.grpbxBillButtonList.Name = "grpbxBillButtonList";
            this.grpbxBillButtonList.Size = new System.Drawing.Size(325, 206);
            this.grpbxBillButtonList.TabIndex = 1;
            this.grpbxBillButtonList.TabStop = false;
            // 
            // btnBill12
            // 
            this.btnBill12.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill12.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill12.ImageIndex = 1;
            this.btnBill12.ImageList = this.Icon30;
            this.btnBill12.Location = new System.Drawing.Point(245, 141);
            this.btnBill12.Name = "btnBill12";
            this.btnBill12.Size = new System.Drawing.Size(74, 55);
            this.btnBill12.TabIndex = 56;
            this.btnBill12.Text = "Hoá đơn 12";
            this.btnBill12.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill12.UseVisualStyleBackColor = true;
            this.btnBill12.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // btnBill11
            // 
            this.btnBill11.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill11.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill11.ImageIndex = 1;
            this.btnBill11.ImageList = this.Icon30;
            this.btnBill11.Location = new System.Drawing.Point(164, 141);
            this.btnBill11.Name = "btnBill11";
            this.btnBill11.Size = new System.Drawing.Size(74, 55);
            this.btnBill11.TabIndex = 55;
            this.btnBill11.Text = "Hoá đơn 11";
            this.btnBill11.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill11.UseVisualStyleBackColor = true;
            this.btnBill11.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // btnBill10
            // 
            this.btnBill10.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill10.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill10.ImageIndex = 1;
            this.btnBill10.ImageList = this.Icon30;
            this.btnBill10.Location = new System.Drawing.Point(85, 141);
            this.btnBill10.Name = "btnBill10";
            this.btnBill10.Size = new System.Drawing.Size(73, 55);
            this.btnBill10.TabIndex = 54;
            this.btnBill10.Text = "Hoá đơn 10";
            this.btnBill10.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill10.UseVisualStyleBackColor = true;
            this.btnBill10.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // btnBill9
            // 
            this.btnBill9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill9.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill9.ImageIndex = 1;
            this.btnBill9.ImageList = this.Icon30;
            this.btnBill9.Location = new System.Drawing.Point(6, 141);
            this.btnBill9.Name = "btnBill9";
            this.btnBill9.Size = new System.Drawing.Size(75, 55);
            this.btnBill9.TabIndex = 53;
            this.btnBill9.Text = "Hoá đơn 9";
            this.btnBill9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill9.UseVisualStyleBackColor = true;
            this.btnBill9.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // btnBill8
            // 
            this.btnBill8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill8.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill8.ImageIndex = 1;
            this.btnBill8.ImageList = this.Icon30;
            this.btnBill8.Location = new System.Drawing.Point(245, 80);
            this.btnBill8.Name = "btnBill8";
            this.btnBill8.Size = new System.Drawing.Size(74, 55);
            this.btnBill8.TabIndex = 52;
            this.btnBill8.Text = "Hoá đơn 8";
            this.btnBill8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill8.UseVisualStyleBackColor = true;
            this.btnBill8.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // btnBill7
            // 
            this.btnBill7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill7.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill7.ImageIndex = 1;
            this.btnBill7.ImageList = this.Icon30;
            this.btnBill7.Location = new System.Drawing.Point(164, 80);
            this.btnBill7.Name = "btnBill7";
            this.btnBill7.Size = new System.Drawing.Size(74, 55);
            this.btnBill7.TabIndex = 51;
            this.btnBill7.Text = "Hoá đơn 7";
            this.btnBill7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill7.UseVisualStyleBackColor = true;
            this.btnBill7.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // btnBill6
            // 
            this.btnBill6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill6.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill6.ImageIndex = 1;
            this.btnBill6.ImageList = this.Icon30;
            this.btnBill6.Location = new System.Drawing.Point(85, 80);
            this.btnBill6.Name = "btnBill6";
            this.btnBill6.Size = new System.Drawing.Size(73, 55);
            this.btnBill6.TabIndex = 50;
            this.btnBill6.Text = "Hoá đơn 6";
            this.btnBill6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill6.UseVisualStyleBackColor = true;
            this.btnBill6.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // btnBill5
            // 
            this.btnBill5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill5.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill5.ImageIndex = 1;
            this.btnBill5.ImageList = this.Icon30;
            this.btnBill5.Location = new System.Drawing.Point(6, 80);
            this.btnBill5.Name = "btnBill5";
            this.btnBill5.Size = new System.Drawing.Size(75, 55);
            this.btnBill5.TabIndex = 49;
            this.btnBill5.Text = "Hoá đơn 5";
            this.btnBill5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill5.UseVisualStyleBackColor = true;
            this.btnBill5.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // btnBill4
            // 
            this.btnBill4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill4.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill4.ImageIndex = 1;
            this.btnBill4.ImageList = this.Icon30;
            this.btnBill4.Location = new System.Drawing.Point(245, 19);
            this.btnBill4.Name = "btnBill4";
            this.btnBill4.Size = new System.Drawing.Size(74, 55);
            this.btnBill4.TabIndex = 48;
            this.btnBill4.Text = "Hoá đơn 4";
            this.btnBill4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill4.UseVisualStyleBackColor = true;
            this.btnBill4.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // btnBill3
            // 
            this.btnBill3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill3.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill3.ImageIndex = 1;
            this.btnBill3.ImageList = this.Icon30;
            this.btnBill3.Location = new System.Drawing.Point(164, 19);
            this.btnBill3.Name = "btnBill3";
            this.btnBill3.Size = new System.Drawing.Size(74, 55);
            this.btnBill3.TabIndex = 47;
            this.btnBill3.Text = "Hoá đơn 3";
            this.btnBill3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill3.UseVisualStyleBackColor = true;
            this.btnBill3.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // btnBill2
            // 
            this.btnBill2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill2.ImageIndex = 1;
            this.btnBill2.ImageList = this.Icon30;
            this.btnBill2.Location = new System.Drawing.Point(85, 19);
            this.btnBill2.Name = "btnBill2";
            this.btnBill2.Size = new System.Drawing.Size(73, 55);
            this.btnBill2.TabIndex = 46;
            this.btnBill2.Text = "Hoá đơn 2";
            this.btnBill2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill2.UseVisualStyleBackColor = true;
            this.btnBill2.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // btnBill1
            // 
            this.btnBill1.BackColor = System.Drawing.Color.Transparent;
            this.btnBill1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill1.ImageIndex = 1;
            this.btnBill1.ImageList = this.Icon30;
            this.btnBill1.Location = new System.Drawing.Point(6, 19);
            this.btnBill1.Name = "btnBill1";
            this.btnBill1.Size = new System.Drawing.Size(75, 55);
            this.btnBill1.TabIndex = 45;
            this.btnBill1.Text = "Hoá đơn 1";
            this.btnBill1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill1.UseVisualStyleBackColor = false;
            this.btnBill1.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // tpBillList2
            // 
            this.tpBillList2.Controls.Add(this.grpbxBillList2);
            this.tpBillList2.Location = new System.Drawing.Point(4, 22);
            this.tpBillList2.Name = "tpBillList2";
            this.tpBillList2.Padding = new System.Windows.Forms.Padding(3);
            this.tpBillList2.Size = new System.Drawing.Size(337, 209);
            this.tpBillList2.TabIndex = 1;
            this.tpBillList2.Text = "Danh sách 2";
            this.tpBillList2.UseVisualStyleBackColor = true;
            // 
            // grpbxBillList2
            // 
            this.grpbxBillList2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grpbxBillList2.Controls.Add(this.btnBill24);
            this.grpbxBillList2.Controls.Add(this.btnBill23);
            this.grpbxBillList2.Controls.Add(this.btnBill22);
            this.grpbxBillList2.Controls.Add(this.btnBill21);
            this.grpbxBillList2.Controls.Add(this.btnBill20);
            this.grpbxBillList2.Controls.Add(this.btnBill19);
            this.grpbxBillList2.Controls.Add(this.btnBill18);
            this.grpbxBillList2.Controls.Add(this.btnBill17);
            this.grpbxBillList2.Controls.Add(this.btnBill16);
            this.grpbxBillList2.Controls.Add(this.btnBill15);
            this.grpbxBillList2.Controls.Add(this.btnBill14);
            this.grpbxBillList2.Controls.Add(this.btnBill13);
            this.grpbxBillList2.Location = new System.Drawing.Point(3, 8);
            this.grpbxBillList2.Name = "grpbxBillList2";
            this.grpbxBillList2.Size = new System.Drawing.Size(325, 205);
            this.grpbxBillList2.TabIndex = 2;
            this.grpbxBillList2.TabStop = false;
            // 
            // btnBill24
            // 
            this.btnBill24.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill24.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill24.ImageIndex = 1;
            this.btnBill24.ImageList = this.Icon30;
            this.btnBill24.Location = new System.Drawing.Point(245, 141);
            this.btnBill24.Name = "btnBill24";
            this.btnBill24.Size = new System.Drawing.Size(74, 55);
            this.btnBill24.TabIndex = 56;
            this.btnBill24.Text = "Hoá đơn 24";
            this.btnBill24.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill24.UseVisualStyleBackColor = true;
            // 
            // btnBill23
            // 
            this.btnBill23.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill23.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill23.ImageIndex = 1;
            this.btnBill23.ImageList = this.Icon30;
            this.btnBill23.Location = new System.Drawing.Point(164, 141);
            this.btnBill23.Name = "btnBill23";
            this.btnBill23.Size = new System.Drawing.Size(74, 55);
            this.btnBill23.TabIndex = 55;
            this.btnBill23.Text = "Hoá đơn 23";
            this.btnBill23.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill23.UseVisualStyleBackColor = true;
            // 
            // btnBill22
            // 
            this.btnBill22.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill22.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill22.ImageIndex = 1;
            this.btnBill22.ImageList = this.Icon30;
            this.btnBill22.Location = new System.Drawing.Point(85, 141);
            this.btnBill22.Name = "btnBill22";
            this.btnBill22.Size = new System.Drawing.Size(73, 55);
            this.btnBill22.TabIndex = 54;
            this.btnBill22.Text = "Hoá đơn 22";
            this.btnBill22.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill22.UseVisualStyleBackColor = true;
            // 
            // btnBill21
            // 
            this.btnBill21.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill21.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill21.ImageIndex = 1;
            this.btnBill21.ImageList = this.Icon30;
            this.btnBill21.Location = new System.Drawing.Point(6, 141);
            this.btnBill21.Name = "btnBill21";
            this.btnBill21.Size = new System.Drawing.Size(75, 55);
            this.btnBill21.TabIndex = 53;
            this.btnBill21.Text = "Hoá đơn 21";
            this.btnBill21.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill21.UseVisualStyleBackColor = true;
            // 
            // btnBill20
            // 
            this.btnBill20.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill20.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill20.ImageIndex = 1;
            this.btnBill20.ImageList = this.Icon30;
            this.btnBill20.Location = new System.Drawing.Point(245, 80);
            this.btnBill20.Name = "btnBill20";
            this.btnBill20.Size = new System.Drawing.Size(74, 55);
            this.btnBill20.TabIndex = 52;
            this.btnBill20.Text = "Hoá đơn 20";
            this.btnBill20.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill20.UseVisualStyleBackColor = true;
            // 
            // btnBill19
            // 
            this.btnBill19.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill19.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill19.ImageIndex = 1;
            this.btnBill19.ImageList = this.Icon30;
            this.btnBill19.Location = new System.Drawing.Point(164, 80);
            this.btnBill19.Name = "btnBill19";
            this.btnBill19.Size = new System.Drawing.Size(74, 55);
            this.btnBill19.TabIndex = 51;
            this.btnBill19.Text = "Hoá đơn 19";
            this.btnBill19.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill19.UseVisualStyleBackColor = true;
            // 
            // btnBill18
            // 
            this.btnBill18.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill18.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill18.ImageIndex = 1;
            this.btnBill18.ImageList = this.Icon30;
            this.btnBill18.Location = new System.Drawing.Point(85, 80);
            this.btnBill18.Name = "btnBill18";
            this.btnBill18.Size = new System.Drawing.Size(73, 55);
            this.btnBill18.TabIndex = 50;
            this.btnBill18.Text = "Hoá đơn 18";
            this.btnBill18.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill18.UseVisualStyleBackColor = true;
            // 
            // btnBill17
            // 
            this.btnBill17.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill17.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill17.ImageIndex = 1;
            this.btnBill17.ImageList = this.Icon30;
            this.btnBill17.Location = new System.Drawing.Point(6, 80);
            this.btnBill17.Name = "btnBill17";
            this.btnBill17.Size = new System.Drawing.Size(75, 55);
            this.btnBill17.TabIndex = 49;
            this.btnBill17.Text = "Hoá đơn 17";
            this.btnBill17.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill17.UseVisualStyleBackColor = true;
            // 
            // btnBill16
            // 
            this.btnBill16.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill16.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill16.ImageIndex = 1;
            this.btnBill16.ImageList = this.Icon30;
            this.btnBill16.Location = new System.Drawing.Point(245, 19);
            this.btnBill16.Name = "btnBill16";
            this.btnBill16.Size = new System.Drawing.Size(74, 55);
            this.btnBill16.TabIndex = 48;
            this.btnBill16.Text = "Hoá đơn 16";
            this.btnBill16.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill16.UseVisualStyleBackColor = true;
            // 
            // btnBill15
            // 
            this.btnBill15.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill15.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill15.ImageIndex = 1;
            this.btnBill15.ImageList = this.Icon30;
            this.btnBill15.Location = new System.Drawing.Point(164, 19);
            this.btnBill15.Name = "btnBill15";
            this.btnBill15.Size = new System.Drawing.Size(74, 55);
            this.btnBill15.TabIndex = 47;
            this.btnBill15.Text = "Hoá đơn 15";
            this.btnBill15.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill15.UseVisualStyleBackColor = true;
            // 
            // btnBill14
            // 
            this.btnBill14.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill14.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill14.ImageIndex = 1;
            this.btnBill14.ImageList = this.Icon30;
            this.btnBill14.Location = new System.Drawing.Point(85, 19);
            this.btnBill14.Name = "btnBill14";
            this.btnBill14.Size = new System.Drawing.Size(73, 55);
            this.btnBill14.TabIndex = 46;
            this.btnBill14.Text = "Hoá đơn 14";
            this.btnBill14.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill14.UseVisualStyleBackColor = true;
            // 
            // btnBill13
            // 
            this.btnBill13.BackColor = System.Drawing.Color.Transparent;
            this.btnBill13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBill13.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBill13.ImageIndex = 1;
            this.btnBill13.ImageList = this.Icon30;
            this.btnBill13.Location = new System.Drawing.Point(6, 19);
            this.btnBill13.Name = "btnBill13";
            this.btnBill13.Size = new System.Drawing.Size(75, 55);
            this.btnBill13.TabIndex = 45;
            this.btnBill13.Text = "Hoá đơn 13";
            this.btnBill13.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBill13.UseVisualStyleBackColor = false;
            this.btnBill13.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // grpBill
            // 
            this.grpBill.Controls.Add(this.lblBillTitle);
            this.grpBill.Controls.Add(this.gbGiaTriHD);
            this.grpBill.Controls.Add(this.btnBillRefresh);
            this.grpBill.Controls.Add(this.btnBillPrinting);
            this.grpBill.Controls.Add(this.grpbxCustomerInfo);
            this.grpBill.Controls.Add(this.btnSettlePay);
            this.grpBill.Controls.Add(this.gridBillDetail);
            this.grpBill.Controls.Add(this.label20);
            this.grpBill.Location = new System.Drawing.Point(351, 6);
            this.grpBill.Name = "grpBill";
            this.grpBill.Size = new System.Drawing.Size(892, 578);
            this.grpBill.TabIndex = 50;
            this.grpBill.TabStop = false;
            // 
            // lblBillTitle
            // 
            this.lblBillTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBillTitle.AutoSize = true;
            this.lblBillTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 31F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillTitle.ForeColor = System.Drawing.Color.Blue;
            this.lblBillTitle.Location = new System.Drawing.Point(319, 8);
            this.lblBillTitle.Name = "lblBillTitle";
            this.lblBillTitle.Size = new System.Drawing.Size(183, 48);
            this.lblBillTitle.TabIndex = 4;
            this.lblBillTitle.Text = "Hoá đơn";
            // 
            // gbGiaTriHD
            // 
            this.gbGiaTriHD.Controls.Add(this.btnCalculateSum);
            this.gbGiaTriHD.Controls.Add(this.txtActualTotal);
            this.gbGiaTriHD.Controls.Add(this.txtDiscountAmount);
            this.gbGiaTriHD.Controls.Add(this.txtTotal);
            this.gbGiaTriHD.Controls.Add(this.lblActualTotal);
            this.gbGiaTriHD.Controls.Add(this.lblDiscountAmount);
            this.gbGiaTriHD.Controls.Add(this.lblTotal);
            this.gbGiaTriHD.Location = new System.Drawing.Point(254, 435);
            this.gbGiaTriHD.Name = "gbGiaTriHD";
            this.gbGiaTriHD.Size = new System.Drawing.Size(345, 105);
            this.gbGiaTriHD.TabIndex = 56;
            this.gbGiaTriHD.TabStop = false;
            // 
            // btnCalculateSum
            // 
            this.btnCalculateSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalculateSum.Location = new System.Drawing.Point(296, 13);
            this.btnCalculateSum.Name = "btnCalculateSum";
            this.btnCalculateSum.Size = new System.Drawing.Size(43, 86);
            this.btnCalculateSum.TabIndex = 50;
            this.btnCalculateSum.Text = "Tính tổng";
            this.btnCalculateSum.UseVisualStyleBackColor = true;
            this.btnCalculateSum.Visible = false;
            this.btnCalculateSum.Click += new System.EventHandler(this.btnCalculateSum_Click);
            // 
            // txtActualTotal
            // 
            this.txtActualTotal.Enabled = false;
            this.txtActualTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualTotal.Location = new System.Drawing.Point(104, 74);
            this.txtActualTotal.Name = "txtActualTotal";
            this.txtActualTotal.Size = new System.Drawing.Size(186, 26);
            this.txtActualTotal.TabIndex = 49;
            this.txtActualTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtDiscountAmount
            // 
            this.txtDiscountAmount.Enabled = false;
            this.txtDiscountAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscountAmount.Location = new System.Drawing.Point(104, 42);
            this.txtDiscountAmount.Name = "txtDiscountAmount";
            this.txtDiscountAmount.Size = new System.Drawing.Size(186, 26);
            this.txtDiscountAmount.TabIndex = 48;
            this.txtDiscountAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotal
            // 
            this.txtTotal.Enabled = false;
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(104, 13);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(186, 26);
            this.txtTotal.TabIndex = 47;
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblActualTotal
            // 
            this.lblActualTotal.AutoSize = true;
            this.lblActualTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActualTotal.Location = new System.Drawing.Point(2, 77);
            this.lblActualTotal.Name = "lblActualTotal";
            this.lblActualTotal.Size = new System.Drawing.Size(99, 20);
            this.lblActualTotal.TabIndex = 46;
            this.lblActualTotal.Text = "Thành tiền:";
            // 
            // lblDiscountAmount
            // 
            this.lblDiscountAmount.AutoSize = true;
            this.lblDiscountAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscountAmount.Location = new System.Drawing.Point(2, 45);
            this.lblDiscountAmount.Name = "lblDiscountAmount";
            this.lblDiscountAmount.Size = new System.Drawing.Size(89, 20);
            this.lblDiscountAmount.TabIndex = 45;
            this.lblDiscountAmount.Text = "Chiết khấu:";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(2, 16);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(89, 20);
            this.lblTotal.TabIndex = 44;
            this.lblTotal.Text = "Tổng tiền:";
            // 
            // btnBillRefresh
            // 
            this.btnBillRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBillRefresh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBillRefresh.ImageKey = "(none)";
            this.btnBillRefresh.ImageList = this.Icon50;
            this.btnBillRefresh.Location = new System.Drawing.Point(688, 520);
            this.btnBillRefresh.Name = "btnBillRefresh";
            this.btnBillRefresh.Size = new System.Drawing.Size(99, 29);
            this.btnBillRefresh.TabIndex = 48;
            this.btnBillRefresh.Text = "Tạo mới HĐ";
            this.btnBillRefresh.UseVisualStyleBackColor = true;
            this.btnBillRefresh.Click += new System.EventHandler(this.btnBillRefresh_Click);
            // 
            // btnBillPrinting
            // 
            this.btnBillPrinting.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBillPrinting.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBillPrinting.ImageKey = "Bill_icon.png";
            this.btnBillPrinting.ImageList = this.Icon50;
            this.btnBillPrinting.Location = new System.Drawing.Point(743, 437);
            this.btnBillPrinting.Name = "btnBillPrinting";
            this.btnBillPrinting.Size = new System.Drawing.Size(143, 77);
            this.btnBillPrinting.TabIndex = 55;
            this.btnBillPrinting.Text = "In hoá đơn tạm";
            this.btnBillPrinting.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBillPrinting.UseVisualStyleBackColor = true;
            this.btnBillPrinting.Click += new System.EventHandler(this.btnBillPrinting_Click);
            // 
            // grpbxCustomerInfo
            // 
            this.grpbxCustomerInfo.Controls.Add(this.btnCustomerDetail);
            this.grpbxCustomerInfo.Controls.Add(this.btnChangeCustomerID);
            this.grpbxCustomerInfo.Controls.Add(this.txtDiscountRate);
            this.grpbxCustomerInfo.Controls.Add(this.txtCustomerID);
            this.grpbxCustomerInfo.Controls.Add(this.lblDiscount);
            this.grpbxCustomerInfo.Controls.Add(this.lblCustomerID);
            this.grpbxCustomerInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpbxCustomerInfo.ForeColor = System.Drawing.Color.Blue;
            this.grpbxCustomerInfo.Location = new System.Drawing.Point(10, 435);
            this.grpbxCustomerInfo.Name = "grpbxCustomerInfo";
            this.grpbxCustomerInfo.Size = new System.Drawing.Size(238, 102);
            this.grpbxCustomerInfo.TabIndex = 31;
            this.grpbxCustomerInfo.TabStop = false;
            this.grpbxCustomerInfo.Text = "Thông tin khách hàng";
            // 
            // btnCustomerDetail
            // 
            this.btnCustomerDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomerDetail.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCustomerDetail.Location = new System.Drawing.Point(85, 74);
            this.btnCustomerDetail.Name = "btnCustomerDetail";
            this.btnCustomerDetail.Size = new System.Drawing.Size(75, 23);
            this.btnCustomerDetail.TabIndex = 57;
            this.btnCustomerDetail.Text = "Xem chi tiết";
            this.btnCustomerDetail.UseVisualStyleBackColor = true;
            this.btnCustomerDetail.Click += new System.EventHandler(this.btnCustomerDetail_Click);
            // 
            // btnChangeCustomerID
            // 
            this.btnChangeCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangeCustomerID.ForeColor = System.Drawing.Color.Black;
            this.btnChangeCustomerID.Location = new System.Drawing.Point(184, 22);
            this.btnChangeCustomerID.Name = "btnChangeCustomerID";
            this.btnChangeCustomerID.Size = new System.Drawing.Size(47, 24);
            this.btnChangeCustomerID.TabIndex = 4;
            this.btnChangeCustomerID.Text = "Đổi";
            this.btnChangeCustomerID.UseVisualStyleBackColor = true;
            this.btnChangeCustomerID.Click += new System.EventHandler(this.btnChangeCustomerID_Click);
            // 
            // txtDiscountRate
            // 
            this.txtDiscountRate.Enabled = false;
            this.txtDiscountRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscountRate.ForeColor = System.Drawing.Color.Black;
            this.txtDiscountRate.Location = new System.Drawing.Point(85, 51);
            this.txtDiscountRate.Name = "txtDiscountRate";
            this.txtDiscountRate.Size = new System.Drawing.Size(84, 20);
            this.txtDiscountRate.TabIndex = 3;
            this.txtDiscountRate.Text = "0";
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerID.ForeColor = System.Drawing.Color.Black;
            this.txtCustomerID.Location = new System.Drawing.Point(85, 25);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.Size = new System.Drawing.Size(84, 20);
            this.txtCustomerID.TabIndex = 2;
            this.txtCustomerID.Text = "1";
            this.txtCustomerID.Enter += new System.EventHandler(this.txtCustomerID_Enter);
            this.txtCustomerID.Leave += new System.EventHandler(this.txtCustomerID_Leave);
            // 
            // lblDiscount
            // 
            this.lblDiscount.AutoSize = true;
            this.lblDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscount.ForeColor = System.Drawing.Color.Black;
            this.lblDiscount.Location = new System.Drawing.Point(4, 54);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(68, 13);
            this.lblDiscount.TabIndex = 1;
            this.lblDiscount.Text = "% chiết khấu";
            // 
            // lblCustomerID
            // 
            this.lblCustomerID.AutoSize = true;
            this.lblCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerID.ForeColor = System.Drawing.Color.Black;
            this.lblCustomerID.Location = new System.Drawing.Point(6, 28);
            this.lblCustomerID.Name = "lblCustomerID";
            this.lblCustomerID.Size = new System.Drawing.Size(54, 13);
            this.lblCustomerID.TabIndex = 0;
            this.lblCustomerID.Text = "Mã số KH";
            // 
            // btnSettlePay
            // 
            this.btnSettlePay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettlePay.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSettlePay.ImageKey = "money-circle-green-3-512.png";
            this.btnSettlePay.ImageList = this.Icon50;
            this.btnSettlePay.Location = new System.Drawing.Point(605, 437);
            this.btnSettlePay.Name = "btnSettlePay";
            this.btnSettlePay.Size = new System.Drawing.Size(132, 77);
            this.btnSettlePay.TabIndex = 47;
            this.btnSettlePay.Text = "Thanh toán ";
            this.btnSettlePay.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSettlePay.UseVisualStyleBackColor = true;
            this.btnSettlePay.Click += new System.EventHandler(this.btnSettlePay_Click);
            // 
            // gridBillDetail
            // 
            this.gridBillDetail.AllowUserToAddRows = false;
            this.gridBillDetail.AllowUserToDeleteRows = false;
            this.gridBillDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridBillDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridBillDetail.Location = new System.Drawing.Point(23, 59);
            this.gridBillDetail.MultiSelect = false;
            this.gridBillDetail.Name = "gridBillDetail";
            this.gridBillDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridBillDetail.Size = new System.Drawing.Size(863, 366);
            this.gridBillDetail.TabIndex = 42;
            this.gridBillDetail.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridBillDetail_CellEndEdit);
            this.gridBillDetail.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gridBillDetail_MouseClick);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(224, -46);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(101, 26);
            this.label20.TabIndex = 43;
            this.label20.Text = "Hoá đơn";
            // 
            // tabAddToBill
            // 
            this.tabAddToBill.Controls.Add(this.tabAddFood);
            this.tabAddToBill.Controls.Add(this.tabAddBeverage);
            this.tabAddToBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabAddToBill.Location = new System.Drawing.Point(3, 255);
            this.tabAddToBill.Name = "tabAddToBill";
            this.tabAddToBill.SelectedIndex = 0;
            this.tabAddToBill.Size = new System.Drawing.Size(342, 329);
            this.tabAddToBill.TabIndex = 56;
            // 
            // tabAddFood
            // 
            this.tabAddFood.Controls.Add(this.btnFoodSearching);
            this.tabAddFood.Controls.Add(this.txtFoodSearching);
            this.tabAddFood.Controls.Add(this.lblFoodSearching);
            this.tabAddFood.Controls.Add(this.btnAddFood);
            this.tabAddFood.Controls.Add(this.txtFoodQuantity);
            this.tabAddFood.Controls.Add(this.lblAddFood);
            this.tabAddFood.Controls.Add(this.gridFood);
            this.tabAddFood.Location = new System.Drawing.Point(4, 22);
            this.tabAddFood.Name = "tabAddFood";
            this.tabAddFood.Padding = new System.Windows.Forms.Padding(3);
            this.tabAddFood.Size = new System.Drawing.Size(334, 303);
            this.tabAddFood.TabIndex = 0;
            this.tabAddFood.Text = "Thêm món ăn";
            this.tabAddFood.UseVisualStyleBackColor = true;
            // 
            // btnFoodSearching
            // 
            this.btnFoodSearching.ImageKey = "Search.png";
            this.btnFoodSearching.ImageList = this.Icon10;
            this.btnFoodSearching.Location = new System.Drawing.Point(241, 12);
            this.btnFoodSearching.Name = "btnFoodSearching";
            this.btnFoodSearching.Size = new System.Drawing.Size(58, 23);
            this.btnFoodSearching.TabIndex = 6;
            this.btnFoodSearching.UseVisualStyleBackColor = true;
            this.btnFoodSearching.Click += new System.EventHandler(this.btnFoodSearching_Click);
            // 
            // txtFoodSearching
            // 
            this.txtFoodSearching.Location = new System.Drawing.Point(65, 14);
            this.txtFoodSearching.Name = "txtFoodSearching";
            this.txtFoodSearching.Size = new System.Drawing.Size(170, 20);
            this.txtFoodSearching.TabIndex = 5;
            this.txtFoodSearching.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFoodSearching_KeyPress);
            // 
            // lblFoodSearching
            // 
            this.lblFoodSearching.AutoSize = true;
            this.lblFoodSearching.Location = new System.Drawing.Point(15, 17);
            this.lblFoodSearching.Name = "lblFoodSearching";
            this.lblFoodSearching.Size = new System.Drawing.Size(52, 13);
            this.lblFoodSearching.TabIndex = 4;
            this.lblFoodSearching.Text = "Tìm kiếm:";
            // 
            // btnAddFood
            // 
            this.btnAddFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddFood.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAddFood.ImageKey = "BBQ.png";
            this.btnAddFood.ImageList = this.Icon30;
            this.btnAddFood.Location = new System.Drawing.Point(241, 249);
            this.btnAddFood.Name = "btnAddFood";
            this.btnAddFood.Size = new System.Drawing.Size(74, 50);
            this.btnAddFood.TabIndex = 3;
            this.btnAddFood.Text = "Thêm món";
            this.btnAddFood.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAddFood.UseVisualStyleBackColor = true;
            this.btnAddFood.Click += new System.EventHandler(this.btnAddFood_Click);
            // 
            // txtFoodQuantity
            // 
            this.txtFoodQuantity.Location = new System.Drawing.Point(175, 267);
            this.txtFoodQuantity.Name = "txtFoodQuantity";
            this.txtFoodQuantity.Size = new System.Drawing.Size(50, 20);
            this.txtFoodQuantity.TabIndex = 2;
            this.txtFoodQuantity.Text = "1";
            // 
            // lblAddFood
            // 
            this.lblAddFood.AutoSize = true;
            this.lblAddFood.Location = new System.Drawing.Point(120, 270);
            this.lblAddFood.Name = "lblAddFood";
            this.lblAddFood.Size = new System.Drawing.Size(52, 13);
            this.lblAddFood.TabIndex = 1;
            this.lblAddFood.Text = "Số lượng:";
            // 
            // gridFood
            // 
            this.gridFood.AllowUserToAddRows = false;
            this.gridFood.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridFood.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridFood.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridFood.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridFood.Location = new System.Drawing.Point(6, 41);
            this.gridFood.MultiSelect = false;
            this.gridFood.Name = "gridFood";
            this.gridFood.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridFood.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridFood.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridFood.Size = new System.Drawing.Size(309, 203);
            this.gridFood.TabIndex = 0;
            // 
            // tabAddBeverage
            // 
            this.tabAddBeverage.Controls.Add(this.btnBeverageSearch);
            this.tabAddBeverage.Controls.Add(this.txtBeverageSearch);
            this.tabAddBeverage.Controls.Add(this.lblBeverageSearch);
            this.tabAddBeverage.Controls.Add(this.btnAddBeverage);
            this.tabAddBeverage.Controls.Add(this.txtBeverageQuantity);
            this.tabAddBeverage.Controls.Add(this.lblBeverageQuantity);
            this.tabAddBeverage.Controls.Add(this.gridBeverage);
            this.tabAddBeverage.Location = new System.Drawing.Point(4, 22);
            this.tabAddBeverage.Name = "tabAddBeverage";
            this.tabAddBeverage.Padding = new System.Windows.Forms.Padding(3);
            this.tabAddBeverage.Size = new System.Drawing.Size(334, 303);
            this.tabAddBeverage.TabIndex = 1;
            this.tabAddBeverage.Text = "Thêm thức uống";
            this.tabAddBeverage.UseVisualStyleBackColor = true;
            // 
            // btnBeverageSearch
            // 
            this.btnBeverageSearch.ImageKey = "Search.png";
            this.btnBeverageSearch.ImageList = this.Icon10;
            this.btnBeverageSearch.Location = new System.Drawing.Point(243, 4);
            this.btnBeverageSearch.Name = "btnBeverageSearch";
            this.btnBeverageSearch.Size = new System.Drawing.Size(58, 23);
            this.btnBeverageSearch.TabIndex = 10;
            this.btnBeverageSearch.UseVisualStyleBackColor = true;
            this.btnBeverageSearch.Click += new System.EventHandler(this.btnBeverageSearch_Click);
            // 
            // txtBeverageSearch
            // 
            this.txtBeverageSearch.Location = new System.Drawing.Point(67, 6);
            this.txtBeverageSearch.Name = "txtBeverageSearch";
            this.txtBeverageSearch.Size = new System.Drawing.Size(170, 20);
            this.txtBeverageSearch.TabIndex = 9;
            this.txtBeverageSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBeverageSearch_KeyPress);
            // 
            // lblBeverageSearch
            // 
            this.lblBeverageSearch.AutoSize = true;
            this.lblBeverageSearch.Location = new System.Drawing.Point(17, 9);
            this.lblBeverageSearch.Name = "lblBeverageSearch";
            this.lblBeverageSearch.Size = new System.Drawing.Size(52, 13);
            this.lblBeverageSearch.TabIndex = 8;
            this.lblBeverageSearch.Text = "Tìm kiếm:";
            // 
            // btnAddBeverage
            // 
            this.btnAddBeverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddBeverage.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAddBeverage.ImageKey = "Drink.jpg";
            this.btnAddBeverage.ImageList = this.Icon30;
            this.btnAddBeverage.Location = new System.Drawing.Point(211, 221);
            this.btnAddBeverage.Name = "btnAddBeverage";
            this.btnAddBeverage.Size = new System.Drawing.Size(110, 57);
            this.btnAddBeverage.TabIndex = 7;
            this.btnAddBeverage.Text = "Thêm thức uống";
            this.btnAddBeverage.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAddBeverage.UseVisualStyleBackColor = true;
            this.btnAddBeverage.Click += new System.EventHandler(this.btnAddBeverage_Click);
            // 
            // txtBeverageQuantity
            // 
            this.txtBeverageQuantity.Location = new System.Drawing.Point(155, 239);
            this.txtBeverageQuantity.Name = "txtBeverageQuantity";
            this.txtBeverageQuantity.Size = new System.Drawing.Size(50, 20);
            this.txtBeverageQuantity.TabIndex = 6;
            this.txtBeverageQuantity.Text = "1";
            // 
            // lblBeverageQuantity
            // 
            this.lblBeverageQuantity.AutoSize = true;
            this.lblBeverageQuantity.Location = new System.Drawing.Point(100, 242);
            this.lblBeverageQuantity.Name = "lblBeverageQuantity";
            this.lblBeverageQuantity.Size = new System.Drawing.Size(52, 13);
            this.lblBeverageQuantity.TabIndex = 5;
            this.lblBeverageQuantity.Text = "Số lượng:";
            // 
            // gridBeverage
            // 
            this.gridBeverage.AllowUserToAddRows = false;
            this.gridBeverage.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridBeverage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridBeverage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridBeverage.DefaultCellStyle = dataGridViewCellStyle5;
            this.gridBeverage.Location = new System.Drawing.Point(6, 32);
            this.gridBeverage.MultiSelect = false;
            this.gridBeverage.Name = "gridBeverage";
            this.gridBeverage.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridBeverage.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.gridBeverage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridBeverage.Size = new System.Drawing.Size(315, 183);
            this.gridBeverage.TabIndex = 4;
            // 
            // tabMenu
            // 
            this.tabMenu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMenu.Controls.Add(this.tabBillManagement);
            this.tabMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabMenu.Location = new System.Drawing.Point(9, 99);
            this.tabMenu.Name = "tabMenu";
            this.tabMenu.SelectedIndex = 0;
            this.tabMenu.Size = new System.Drawing.Size(1344, 630);
            this.tabMenu.TabIndex = 2;
            // 
            // frmQLNHDN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmQLNHDN";
            this.Text = "Quản lý nhà hàng BIT";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQLNHDN_FormClosing);
            this.Load += new System.EventHandler(this.frmQLNHDN_Load);
            this.contextBillItemSelection.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuTopBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.tabBillManagement.ResumeLayout(false);
            this.tabBillList.ResumeLayout(false);
            this.tpBillList1.ResumeLayout(false);
            this.grpbxBillButtonList.ResumeLayout(false);
            this.tpBillList2.ResumeLayout(false);
            this.grpbxBillList2.ResumeLayout(false);
            this.grpBill.ResumeLayout(false);
            this.grpBill.PerformLayout();
            this.gbGiaTriHD.ResumeLayout(false);
            this.gbGiaTriHD.PerformLayout();
            this.grpbxCustomerInfo.ResumeLayout(false);
            this.grpbxCustomerInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBillDetail)).EndInit();
            this.tabAddToBill.ResumeLayout(false);
            this.tabAddFood.ResumeLayout(false);
            this.tabAddFood.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridFood)).EndInit();
            this.tabAddBeverage.ResumeLayout(false);
            this.tabAddBeverage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBeverage)).EndInit();
            this.tabMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbNhanVien;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbGioHienTai;
        private System.Windows.Forms.Label lbNgayHienTai;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ImageList Icon50;
        private System.Windows.Forms.ImageList Icon30;
        private System.Windows.Forms.ContextMenuStrip menuTopBar;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ContextMenuStrip contextBillItemSelection;
        private System.Windows.Forms.ToolStripMenuItem itemDelete;
        private System.Windows.Forms.ImageList Icon10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Drawing.Printing.PrintDocument printDoc;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblXemTKTheo;
        private System.Windows.Forms.TabControl tabMenu;
        private System.Windows.Forms.TabPage tabBillManagement;
        private System.Windows.Forms.TabControl tabBillList;
        private System.Windows.Forms.TabPage tpBillList1;
        private System.Windows.Forms.GroupBox grpbxBillButtonList;
        private System.Windows.Forms.Button btnBill12;
        private System.Windows.Forms.Button btnBill11;
        private System.Windows.Forms.Button btnBill10;
        private System.Windows.Forms.Button btnBill9;
        private System.Windows.Forms.Button btnBill8;
        private System.Windows.Forms.Button btnBill7;
        private System.Windows.Forms.Button btnBill6;
        private System.Windows.Forms.Button btnBill5;
        private System.Windows.Forms.Button btnBill4;
        private System.Windows.Forms.Button btnBill3;
        private System.Windows.Forms.Button btnBill2;
        private System.Windows.Forms.Button btnBill1;
        private System.Windows.Forms.TabPage tpBillList2;
        private System.Windows.Forms.GroupBox grpbxBillList2;
        private System.Windows.Forms.Button btnBill24;
        private System.Windows.Forms.Button btnBill23;
        private System.Windows.Forms.Button btnBill22;
        private System.Windows.Forms.Button btnBill21;
        private System.Windows.Forms.Button btnBill20;
        private System.Windows.Forms.Button btnBill19;
        private System.Windows.Forms.Button btnBill18;
        private System.Windows.Forms.Button btnBill17;
        private System.Windows.Forms.Button btnBill16;
        private System.Windows.Forms.Button btnBill15;
        private System.Windows.Forms.Button btnBill14;
        private System.Windows.Forms.Button btnBill13;
        private System.Windows.Forms.GroupBox grpBill;
        private System.Windows.Forms.Label lblBillTitle;
        private System.Windows.Forms.GroupBox gbGiaTriHD;
        private System.Windows.Forms.Button btnCalculateSum;
        private System.Windows.Forms.TextBox txtActualTotal;
        private System.Windows.Forms.TextBox txtDiscountAmount;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label lblActualTotal;
        private System.Windows.Forms.Label lblDiscountAmount;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Button btnBillRefresh;
        private System.Windows.Forms.Button btnBillPrinting;
        private System.Windows.Forms.GroupBox grpbxCustomerInfo;
        private System.Windows.Forms.Button btnCustomerDetail;
        private System.Windows.Forms.Button btnChangeCustomerID;
        private System.Windows.Forms.TextBox txtDiscountRate;
        private System.Windows.Forms.TextBox txtCustomerID;
        private System.Windows.Forms.Label lblDiscount;
        private System.Windows.Forms.Label lblCustomerID;
        private System.Windows.Forms.Button btnSettlePay;
        private System.Windows.Forms.DataGridView gridBillDetail;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TabControl tabAddToBill;
        private System.Windows.Forms.TabPage tabAddFood;
        private System.Windows.Forms.Button btnFoodSearching;
        private System.Windows.Forms.TextBox txtFoodSearching;
        private System.Windows.Forms.Label lblFoodSearching;
        private System.Windows.Forms.Button btnAddFood;
        private System.Windows.Forms.TextBox txtFoodQuantity;
        private System.Windows.Forms.Label lblAddFood;
        private System.Windows.Forms.DataGridView gridFood;
        private System.Windows.Forms.TabPage tabAddBeverage;
        private System.Windows.Forms.Button btnBeverageSearch;
        private System.Windows.Forms.TextBox txtBeverageSearch;
        private System.Windows.Forms.Label lblBeverageSearch;
        private System.Windows.Forms.Button btnAddBeverage;
        private System.Windows.Forms.TextBox txtBeverageQuantity;
        private System.Windows.Forms.Label lblBeverageQuantity;
        private System.Windows.Forms.DataGridView gridBeverage;
    }
}

