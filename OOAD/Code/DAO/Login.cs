﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace DAO
{
    public class Login
    {
        public static Tuple<DataTable, int> CheckLogin(string tendangnhap, string matkhau)
        {
            var cn = DBConnection.connectDB();
            using (SqlCommand cmd = new SqlCommand("sp_CheckLogin", cn)
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                cmd.Parameters.Add("@tendangnhap", SqlDbType.Char);
                cmd.Parameters.Add("@matkhau", SqlDbType.Char);
                cmd.Parameters.Add("@check", SqlDbType.Int).Direction = ParameterDirection.Output;

                cmd.Parameters["@tendangnhap"].Value = tendangnhap;
                cmd.Parameters["@matkhau"].Value = matkhau;
                cmd.Parameters["@check"].Value = -1;

                cmd.ExecuteNonQuery();

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    var dt = new DataTable();
                    da.Fill(dt);

                    cn.Close();

                    return Tuple.Create(dt, Convert.ToInt32(cmd.Parameters["@check"].Value));
                }
            }
        }

        public static int CheckUsername(string tendangnhap)
        {
            var cn = DBConnection.connectDB();
            using (SqlCommand cmd = new SqlCommand("sp_CheckUsername", cn)
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                cmd.Parameters.Add("@tendangnhap", SqlDbType.Char);
                cmd.Parameters.Add("@check", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters["@tendangnhap"].Value = tendangnhap;
                cmd.Parameters["@check"].Value = -1;

                cmd.ExecuteNonQuery();

                cn.Close();

                return Convert.ToInt32(cmd.Parameters["@check"].Value.ToString());
            }
        }
    }
}