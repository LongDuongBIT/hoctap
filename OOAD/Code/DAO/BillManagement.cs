﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAO
{
    public class BillManagement
    {
        public static Bill CreateNewBill(Bill new_bill)
        {
            //Tạo dòng mới trên bảng HoaDon
            using (var cmd = new SqlCommand("sp_createNewBillInfo", DBConnection.connectDB())
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                cmd.Parameters.Add("@NgayGio", SqlDbType.DateTime).Value = new_bill.CreatingTime;
                cmd.Parameters.Add("@MaNV", SqlDbType.Int).Value = new_bill.Staff.ID;
                cmd.Parameters.Add("@MaKH", SqlDbType.Int).Value = new_bill.Customer.ID;
                cmd.Parameters.Add("@TiLeChietKhau", SqlDbType.Float).Value = new_bill.Customer.DiscountRate;
                cmd.Parameters.Add("@MaHD", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                new_bill.ID = cmd.Parameters["@MaHD"].Value.ToString(); //Lấy ra mã HD vừa tạo

                //Tạo các dòng trong bảng CT_HoaDon
                return InsertItemsToBill(new_bill);
            }
        }

        public static Customer GetCustomerDetail(string customerID)
        {
            using (var cmd = new SqlCommand("sp_CustomerDetail", DBConnection.connectDB())
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                cmd.Parameters.Add("@MakH", SqlDbType.NVarChar, 50).Value = customerID;

                using (var da = new SqlDataAdapter(cmd))
                {
                    var dt = new DataTable();
                    da.Fill(dt);

                    var customer = new Customer();
                    //Nếu không tìm được khách hàng thì trả về đối tượng customer rỗng
                    if (dt.Rows.Count == 0)
                        return customer;
                    foreach (DataRow row in dt.Rows)
                    {
                        customer.ID = row.Field<int>("MaKH").ToString();
                        customer.FullName = row.Field<string>("HoTenKH");
                        customer.CivilID = row.Field<string>("CMND");
                        customer.PhoneNumber = row.Field<string>("SoDT");
                        customer.Type = row.Field<string>("TenLoai");
                        customer.DiscountRate = row.Field<double>("TyLeChietKhau");
                        customer.Point = row.Field<int>("DiemTichLuy");
                    }
                    return customer;
                }
            }
        }

        public static List<Product> LoadBeverageList(string name)
        {
            using (var cmd = new SqlCommand("sp_BeverageSearch", DBConnection.connectDB())
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                cmd.Parameters.Add("@TenSP", SqlDbType.NVarChar, 50).Value = name;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    var dt = new DataTable();
                    da.Fill(dt);

                    var beverageList = new List<Product>();
                    foreach (DataRow row in dt.Rows)
                    {
                        var beverage = new Product
                        {
                            ID = row.Field<int>(0).ToString(),
                            Name = row.Field<string>(1),
                            Price = row.Field<int>(2),
                            Type = row.Field<string>(3)
                        };
                        beverageList.Add(beverage);
                    }
                    return beverageList;
                }
            }
        }

        public static List<Product> LoadFoodList(string name)
        {
            using (var cmd = new SqlCommand("sp_FoodSearch", DBConnection.connectDB())
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                cmd.Parameters.Add("@TenSP", SqlDbType.NVarChar, 50).Value = name;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    var dt = new DataTable();
                    da.Fill(dt);

                    var foodList = new List<Product>();
                    foreach (DataRow row in dt.Rows)
                    {
                        var food = new Product
                        {
                            ID = row.Field<int>(0).ToString(),
                            Name = row.Field<string>(1),
                            Price = row.Field<int>(2),
                            Type = row.Field<string>(3)
                        };
                        foodList.Add(food);
                    }
                    return foodList;
                }
            }
        }

        public static void UpdatePoint(Customer customer, int point)
        {
            using (var cmd = new SqlCommand("sp_UpdatePoint", DBConnection.connectDB())
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                cmd.Parameters.AddWithValue("@MaKH", customer.ID);
                cmd.Parameters.AddWithValue("@DiemTichLuy", point);
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpgradeCustomerToVIP(Customer customer)
        {
            using (var cmd = new SqlCommand("sp_UpgradeCustomerToVIP", DBConnection.connectDB())
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                cmd.Parameters.AddWithValue("@MaKH", customer.ID);
                cmd.ExecuteNonQuery();
            }
            //Điểm trở về mức 0
            UpdatePoint(customer, 0);
        }

        private static Bill InsertItemsToBill(Bill new_bill)
        {
            using (SqlCommand cmd = new SqlCommand("sp_insertItemsToBill", DBConnection.connectDB()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MaHD", SqlDbType.Int).Value = new_bill.ID;
                cmd.Parameters.Add("@MaSP", SqlDbType.Int);
                cmd.Parameters.Add("@SoLuong", SqlDbType.Int);
                cmd.Parameters.Add("@GiaBan", SqlDbType.Int);
                cmd.Prepare();
                foreach (DataRow row in new_bill.DetailTable.Rows)
                {
                    cmd.Parameters["@MaSP"].Value = Convert.ToInt32(row.Field<string>("Mã SP"));
                    cmd.Parameters["@SoLuong"].Value = row.Field<int>("Số lượng");
                    cmd.Parameters["@GiaBan"].Value = NumberFromVND(row.Field<string>("Đơn giá"));
                    cmd.ExecuteNonQuery();
                }
                return new_bill;
            }
        }

        //Hàm chuyển đổi từ Tiền tệ thành Số
        private static int NumberFromVND(string currency)
        {
            return int.Parse(currency, System.Globalization.NumberStyles.Currency, new System.Globalization.CultureInfo("vi-VN"));
        }
    }
}